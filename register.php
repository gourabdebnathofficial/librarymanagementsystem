<?php
$servername = "localhost";
$username = "root";
$password = "";
$dbname = "lms";

try {
    // Create connection
    $connection = new mysqli($servername, $username, $password, $dbname);

    // Check connection
    if ($connection->connect_error) {
        throw new Exception("Connection failed: " . $connection->connect_error);
    }

    // Prepare and bind
    $stmt = $connection->prepare("INSERT INTO users (name, email, password, mobile, address) VALUES (?, ?, ?, ?, ?)");
    $stmt->bind_param("sssds", $name, $email, $password, $mobile, $address);

    // Set parameters and execute
    $name = $_POST['name'];
    $email = $_POST['email'];
    $password = $_POST['password'];
    $mobile = $_POST['mobile'];
    $address = $_POST['address'];
    $stmt->execute();

    // Success message
    echo '<script type="text/javascript">
            alert("Registration successful...You may login now !!");
            window.location.href = "index.php";
          </script>';
    
    // Close statement and connection
    $stmt->close();
    $connection->close();
} catch (Exception $e) {
    // Error message
    echo '<script type="text/javascript">
            alert("Error: Email Already Exists");
            window.location.href = "index.php";
          </script>';
}
?>
