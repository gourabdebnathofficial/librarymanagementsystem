<?php
session_start();
?>
<!DOCTYPE html>
<html>

<head>
	<title>Issue Book</title>
	<meta charset="utf-8" name="viewport" content="width=device-width,initial-scale=1">
	<link rel="stylesheet" type="text/css" href="../bootstrap-4.4.1/css/bootstrap.min.css">
	<script type="text/javascript" src="../bootstrap-4.4.1/js/juqery_latest.js"></script>
	<script type="text/javascript" src="../bootstrap-4.4.1/js/bootstrap.min.js"></script>
	<script type="text/javascript">
		function alertMsg() {
			alert("Book added successfully...");
			window.location.href = "admin_dashboard.php";
		}
	</script>
</head>

<body>
	<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
		<div class="container-fluid">
			<div class="navbar-header">
				<a class="navbar-brand" href="admin_dashboard.php">Library Management System (LMS)</a>
			</div>
			<font style="color: white"><span><strong>Welcome: <?php echo $_SESSION['name']; ?></strong></span></font>
			<font style="color: white"><span><strong>Email: <?php echo $_SESSION['email']; ?></strong></span></font>
			<ul class="nav navbar-nav navbar-right">
				<li class="nav-item dropdown">
					<a class="nav-link dropdown-toggle" data-toggle="dropdown">My Profile </a>
					<div class="dropdown-menu">
						<a class="dropdown-item" href="">View Profile</a>
						<div class="dropdown-divider"></div>
						<a class="dropdown-item" href="#">Edit Profile</a>
						<div class="dropdown-divider"></div>
						<a class="dropdown-item" href="change_password.php">Change Password</a>
					</div>
				</li>
				<li class="nav-item">
					<a class="nav-link" href="../logout.php">Logout</a>
				</li>
			</ul>
		</div>
	</nav><br>
	<nav class="navbar navbar-expand-lg navbar-light" style="background-color: #e3f2fd">
		<div class="container-fluid">

			<ul class="nav navbar-nav navbar-center">
				<li class="nav-item">
					<a class="nav-link" href="admin_dashboard.php">Dashboard</a>
				</li>
				<li class="nav-item dropdown">
					<a class="nav-link dropdown-toggle" data-toggle="dropdown">Books </a>
					<div class="dropdown-menu">
						<a class="dropdown-item" href="add_book.php">Add New Book</a>
						<div class="dropdown-divider"></div>
						<a class="dropdown-item" href="manage_book.php">Manage Books</a>
					</div>
				</li>
				<li class="nav-item dropdown">
					<a class="nav-link dropdown-toggle" data-toggle="dropdown">Category </a>
					<div class="dropdown-menu">
						<a class="dropdown-item" href="add_cat.php">Add New Category</a>
						<div class="dropdown-divider"></div>
						<a class="dropdown-item" href="manage_cat.php">Manage Category</a>
					</div>
				</li>
				<li class="nav-item dropdown">
					<a class="nav-link dropdown-toggle" data-toggle="dropdown">Authors</a>
					<div class="dropdown-menu">
						<a class="dropdown-item" href="add_author.php">Add New Author</a>
						<div class="dropdown-divider"></div>
						<a class="dropdown-item" href="manage_author.php">Manage Author</a>
					</div>
				</li>
				<li class="nav-item">
					<a class="nav-link" href="issue_book.php">Issue Book</a>
				</li>
			</ul>
		</div>
	</nav><br>
	<span>
		<marquee>This is library management system. Library opens at 8:00 AM and closes at 8:00 PM</marquee>
	</span><br><br>
	<center>
		<h4>Issue Book</h4><br>
	</center>
	<div class="row">
		<div class="col-md-4"></div>
		<div class="col-md-4">
			<form action="" method="post">
				<div class="form-group">
					<label for="book_name">Book Name:</label>
					<select class="form-control" name="book_name" required>
						<option value="">-Select Book Name-</option>
						<?php
						$connection = mysqli_connect("localhost", "root", "");
						$db = mysqli_select_db($connection, "lms");
						$query = "select book_name from books";
						$query_run = mysqli_query($connection, $query);
						while ($row = mysqli_fetch_assoc($query_run)) {
							echo '<option value="' . $row['book_name'] . '">' . $row['book_name'] . '</option>';
						}
						?>
					</select>
				</div>
				<div class="form-group">
					<label for="book_author">Author Name:</label>
					<select class="form-control" name="book_author" required>
						<option value="">-Select Author-</option>
						<?php
						$query = "select author_name from authors";
						$query_run = mysqli_query($connection, $query);
						while ($row = mysqli_fetch_assoc($query_run)) {
							echo '<option value="' . $row['author_name'] . '">' . $row['author_name'] . '</option>';
						}
						?>
					</select>
				</div>
				<div class="form-group">
					<label for="book_no">Select Book Number:</label>
					<select class="form-control" name="book_no" required>
						<option value="">-Select Book Number-</option>
						<?php
						$query = "SELECT book_no FROM books";
						$query_run = mysqli_query($connection, $query);
						while ($row = mysqli_fetch_assoc($query_run)) {
							echo '<option value="' . $row['book_no'] . '">' . $row['book_no'] . '</option>';
						}
						?>
					</select>
				</div>
				<div class="form-group">
					<label for="student_id">Select Student ID & Name:</label>
					<select class="form-control" name="student_id" required>
						<option value="">-Select ID-</option>
						<?php
						$query = "SELECT id, name FROM users";
						$query_run = mysqli_query($connection, $query);
						while ($row = mysqli_fetch_assoc($query_run)) {
							echo '<option value="' . $row['id'] . '">ID:' . $row['id'] . ' Name: ' . $row['name'] . '</option>';
						}
						?>
					</select>
				</div>
				<div class="form-group">
					<label for="issue_date">Issue Date:</label>
					<input type="date" name="issue_date" class="form-control" value="<?php echo date('Y-m-d'); ?>"
						required>
				</div>
				<div class="form-group">
					<label for="return_date">Return Date:</label>
					<input type="date" name="return_date" class="form-control" value="<?php echo date('Y-m-d', strtotime('+15 days')); ?>">
				</div>
				<button type="submit" name="issue_book" class="btn btn-primary">Issue Book</button>
			</form>
		</div>
		<div class="col-md-4"></div>
	</div>
</body>

</html>
<?php
if (isset($_POST['issue_book'])) {
	$connection = mysqli_connect("localhost", "root", "", "lms");

	// Sanitizing inputs to prevent SQL injection
	$book_no = mysqli_real_escape_string($connection, $_POST['book_no']);
	$book_name = mysqli_real_escape_string($connection, $_POST['book_name']);
	$book_author = mysqli_real_escape_string($connection, $_POST['book_author']);
	$student_id = mysqli_real_escape_string($connection, $_POST['student_id']);
	$issue_date = mysqli_real_escape_string($connection, $_POST['issue_date']);
	$return_date = mysqli_real_escape_string($connection, $_POST['return_date']);
	$query = "INSERT INTO issued_books (book_no, book_name, book_author, student_id, status, issue_date, return_date) VALUES ('$book_no', '$book_name', '$book_author', '$student_id', 1, '$issue_date', '$return_date')";
	$query_run = mysqli_query($connection, $query);

	if ($query_run) {
		echo '<script>alert("Successfully issued the book.");</script>';
		echo '<script>window.location.href = "admin_dashboard.php";</script>';
	} else {
		echo '<script>alert("Failed to issue the book.");</script>';
	}
}
?>