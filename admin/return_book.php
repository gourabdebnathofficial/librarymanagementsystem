<?php
session_start();
$connection = mysqli_connect("localhost", "root", "", "lms");


?>

<!DOCTYPE html>
<html>

<head>
    <title>Return Book</title>
    <meta charset="utf-8" name="viewport" content="width=device-width,initial-scale=1">
    <link rel="stylesheet" type="text/css" href="../bootstrap-4.4.1/css/bootstrap.min.css">
    <script type="text/javascript" src="../bootstrap-4.4.1/js/jquery_latest.js"></script>
    <script type="text/javascript" src="../bootstrap-4.4.1/js/bootstrap.min.js"></script>
</head>

<body>
    <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
        <div class="container-fluid">
            <div class="navbar-header">
                <a class="navbar-brand" href="admin_dashboard.php">Library Management System (LMS)</a>
            </div>
            <font style="color: white"><span><strong>Welcome: <?php echo $_SESSION['name']; ?></strong></span></font>
            <font style="color: white"><span><strong>Email: <?php echo $_SESSION['email']; ?></strong></span></font>
            <ul class="nav navbar-nav navbar-right">
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" data-toggle="dropdown">My Profile</a>
                    <div class="dropdown-menu">
                        <a class="dropdown-item" href="">View Profile</a>
                        <div class="dropdown-divider"></div>
                        <a class="dropdown-item" href="#">Edit Profile</a>
                        <div class="dropdown-divider"></div>
                        <a class="dropdown-item" href="change_password.php">Change Password</a>
                    </div>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="../logout.php">Logout</a>
                </li>
            </ul>
        </div>
    </nav><br>


    <center>
        <h4>Return Book</h4><br>
    </center>
    <div class="row">
        <div class="col-md-4">
            <form action="" method="post">
                <table class="table table-bordered">
                    <tr>
                        <label for="student_id">Select Student ID & Name:</label>
                        <td><select name="stu" id="stu" class="form-control">
                                <option value="">-Select ID-</option>
                                <?php
                                $query = "SELECT student_id FROM issued_books WHERE status=1";
                                $query_run = mysqli_query($connection, $query);
                                while ($row = mysqli_fetch_assoc($query_run)) {
                                    echo '<option value="' . $row['student_id'] . '">ID:' . $row['student_id'] . '</option>';
                                }
                                ?>
                            </select></td>
                        <td><button type="submit" name="search" value="search" class="btn btn-primary">Search
                                Book</button></td>
                    </tr>
                </table>
            </form>
        </div>
    </div>
    <div class="">
                <?php
                    if(isset($_POST["search"])){
                        $query = "SELECT * FROM issued_books WHERE student_id='$_POST[stu]' AND status=1";
                        $query_run = mysqli_query($connection, $query);
                        echo "<table class='table table-bordered'>";
                            echo "<tr>";
                                echo "<th>"; echo"s_no"; echo "</th>";
                                echo "<th>"; echo"book_no"; echo "</th>";
                                echo "<th>"; echo"book_name"; echo "</th>";
                                echo "<th>"; echo"book_author"; echo "</th>";
                                echo "<th>"; echo"student_id"; echo "</th>";
                                echo "<th>"; echo"status"; echo "</th>";
                                echo "<th>"; echo"issue_date"; echo "</th>";
                                echo "<th>"; echo"return_date"; echo "</th>";
                                echo "<th>"; echo"return_books"; echo "</th>";
                            echo "</tr>";
                        while ($row = mysqli_fetch_assoc($query_run)) {
                            echo "<tr>";
                                echo "<td>"; echo $row["s_no"]; echo "</td>";
                                echo "<td>"; echo $row["book_no"]; echo "</td>";
                                echo "<td>"; echo $row["book_name"]; echo "</td>";
                                echo "<td>"; echo $row["book_author"]; echo "</td>";
                                echo "<td>"; echo $row["student_id"]; echo "</td>";
                                echo "<td>"; echo $row["status"]; echo "</td>";
                                echo "<td>"; echo $row["issue_date"]; echo "</td>";
                                echo "<td>"; echo $row["return_date"]; echo "</td>";
                                echo "<td>"; ?> <a href="return.php?student_id=<?php echo $row["s_no"]; ?>">Return Book</a> <?php echo "</td>";
                            echo "</tr>";
                        }
                        echo "</table>";
                    }
                ?>
            </div>
</body>

</html>