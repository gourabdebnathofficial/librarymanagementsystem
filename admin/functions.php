<?php
	function get_author_count(){
		$connection = mysqli_connect("localhost","root","");
		$db = mysqli_select_db($connection,"lms");
		$author_count = 0;
		$query = "select count(*) as author_count from authors";
		$query_run = mysqli_query($connection,$query);
		while ($row = mysqli_fetch_assoc($query_run)){
			$author_count = $row['author_count'];
		}
		return($author_count);
	}

	function get_user_count(){
		$connection = mysqli_connect("localhost","root","");
		$db = mysqli_select_db($connection,"lms");
		$user_count = 0;
		$query = "select count(*) as user_count from users";
		$query_run = mysqli_query($connection,$query);
		while ($row = mysqli_fetch_assoc($query_run)){
			$user_count = $row['user_count'];
		}
		return($user_count);
	}

	function get_book_count(){
		$connection = mysqli_connect("localhost","root","");
		$db = mysqli_select_db($connection,"lms");
		$book_count = 0;
		$query = "select count(*) as book_count from books";
		$query_run = mysqli_query($connection,$query);
		while ($row = mysqli_fetch_assoc($query_run)){
			$book_count = $row['book_count'];
		}
		return($book_count);
	}
	$connection = mysqli_connect("localhost", "root", "", "lms");

	if (!$connection) {
		die("Database connection failed: " . mysqli_connect_error());
	}
	function get_issue_book_count(){
		$connection = mysqli_connect("localhost","root","");
		$db = mysqli_select_db($connection,"lms");
		$issue_book_count = 0;
		$query = "select count(*) as issue_book_count from issued_books";
		$query_run = mysqli_query($connection,$query);
		while ($row = mysqli_fetch_assoc($query_run)){
			$issue_book_count = $row['issue_book_count'];
		}
		return($issue_book_count);
	}

	function get_category_count(){
		$connection = mysqli_connect("localhost","root","");
		$db = mysqli_select_db($connection,"lms");
		$cat_count = 0;
		$query = "select count(*) as cat_count from category";
		$query_run = mysqli_query($connection,$query);
		while ($row = mysqli_fetch_assoc($query_run)){
			$cat_count = $row['cat_count'];
		}
		return($cat_count);
	}

	function request_book($user_id, $book_id) {
		$conn = new mysqli('localhost', 'username', 'password', 'database');
		$stmt = $conn->prepare("INSERT INTO book_requests (user_id, book_id) VALUES (?, ?)");
		$stmt->bind_param("ii", $user_id, $book_id);
		$stmt->execute();
		$stmt->close();
		$conn->close();
	}

	function get_pending_requests() {
		global $connection;
		$query = "SELECT * FROM book_requests WHERE status = 'Pending'";
		$result = mysqli_query($connection, $query);
		$requests = mysqli_fetch_all($result, MYSQLI_ASSOC);
		return $requests;
	}
	
	function update_request_status($request_id, $action) {
		global $connection;
		$status = ($action == 'Approve') ? 'Approved' : 'Denied';
		$query = "UPDATE book_requests SET status = ? WHERE id = ?";
		$stmt = mysqli_prepare($connection, $query);
		mysqli_stmt_bind_param($stmt, "si", $status, $request_id);
		mysqli_stmt_execute($stmt);
		mysqli_stmt_close($stmt);
	}
