<?php
session_start();
$connection = mysqli_connect("localhost", "root", "", "lms");

if (!$connection) {
    die("Database connection failed: " . mysqli_connect_error());
}

if (isset($_POST['request_book'])) {
    $book_id = $_POST['book_name'];
    $student_id = $_SESSION['id'];

    $query = "INSERT INTO book_requests (user_id, book_id, request_date, status) VALUES (?, ?, NOW(), 'Pending')";
    $stmt = mysqli_prepare($connection, $query);
    mysqli_stmt_bind_param($stmt, "ii", $student_id, $book_id);
    
    if (mysqli_stmt_execute($stmt)) {
        echo "<script>alert('Book request submitted successfully.'); window.location.href='user_dashboard.php';</script>";
    } else {
        echo "<script>alert('Failed to submit book request. Please try again.');</script>";
    }
    
    mysqli_stmt_close($stmt);
}

function getOptions($connection, $table, $id_field, $name_field) {
    $options = "";
    $query = "SELECT $id_field, $name_field FROM $table";
    $result = mysqli_query($connection, $query);

    if ($result) {
        while ($row = mysqli_fetch_assoc($result)) {
            $options .= '<option value="' . $row[$id_field] . '">' . $row[$name_field] . '</option>';
        }
    }

    return $options;
}

?>
<!DOCTYPE html>
<html>

<head>
    <title>Request Book</title>
    <meta charset="utf-8" name="viewport" content="width=device-width,initial-scale=1">
    <link rel="stylesheet" type="text/css" href="bootstrap-4.4.1/css/bootstrap.min.css">
    <script type="text/javascript" src="bootstrap-4.4.1/js/jquery_latest.js"></script>
    <script type="text/javascript" src="bootstrap-4.4.1/js/bootstrap.min.js"></script>
</head>

<body>
    <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
        <div class="container-fluid">
            <div class="navbar-header">
                <a class="navbar-brand" href="user_dashboard.php">Library Management System (LMS)</a>
            </div>
            <font style="color: white">
                <span><strong>Welcome: <?php echo $_SESSION['name']; ?></strong></span>
            </font>
            <font style="color: white">
                <span><strong>Email: <?php echo $_SESSION['email']; ?></strong></font>
        </div>
    </nav><br>
    <span>
        <marquee>This is library management system. Library opens at 8:00 AM and closes at 8:00 PM</marquee>
    </span><br><br>
    <center>
        <h4>Request Book Details</h4><br>
    </center>
    <div class="row">
        <div class="col-md-4"></div>
        <div class="col-md-4">
            <form action="" method="post">
                <div class="form-group">
                    <label for="book_name">Book Name:</label>
                    <select class="form-control" name="book_name" required>
                        <option value="">-Select Book-</option>
                        <?php echo getOptions($connection, 'books', 'book_id', 'book_name'); ?>
                    </select>
                </div>
                <div class="form-group">
                    <label for="book_author">Author Name:</label>
                    <select class="form-control" name="book_author" required>
                        <option value="">-Select Author-</option>
                        <?php echo getOptions($connection, 'authors', 'author_id', 'author_name'); ?>
                    </select>
                </div>
                <div class="form-group">
                    <label for="student_id">Student ID:</label>
                    <input type="text" name="student_id" class="form-control" value="<?php echo $_SESSION['id']; ?>"
                        readonly>
                </div>
                <button type="submit" name="request_book" class="btn btn-primary">Request Book</button>
            </form>
        </div>
        <div class="col-md-4"></div>
    </div>
</body>

</html>
<?php mysqli_close($connection); ?>
